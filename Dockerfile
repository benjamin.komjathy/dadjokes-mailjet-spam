FROM python:3.9.19-slim

WORKDIR /app

# set env variables
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
ENV TZ="Europe/Budapest"


# install dependencies
COPY requirements.txt .
RUN pip3 install -r requirements.txt && rm -fv requirements.txt

# copy project
# COPY ./dadjokes.py .

