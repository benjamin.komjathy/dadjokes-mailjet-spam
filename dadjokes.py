import os
import json
import requests
import csv
from mailjet_rest import Client
from dotenv import load_dotenv

load_dotenv()


api_key = os.environ['MJ_APIKEY_PUBLIC']
api_secret = os.environ['MJ_APIKEY_PRIVATE']
email = os.environ['SENDER_EMAIL']
name = os.environ['SENDER_NAME']

mailjet = Client(auth=(api_key, api_secret), version='v3.1')


def get_joke():

    if 'PAID_API' not in os.environ:

        headers = {
            'User-Agent': f'{name} Agent Mailjet 1.0',
            'From': email,
            'Accept': 'application/json'
        }

        response = requests.get("https://icanhazdadjoke.com/", headers=headers)
        json_data = json.loads(response.text)
        joke = json_data['joke']
        return (joke)


def send_email(whotospam_name, whotospam_email, joke):
    data = {
        'Messages': [
            {
                "From": {
                    "Email": email,
                    "Name": name
                },
                "To": [
                    {
                        "Email": whotospam_email,
                        "Name": whotospam_name
                    }
                ],
                "Subject": "Daily D(e)ad jokes",
                "HTMLPart": f"""
                <h3>Your daily dose of happiness!</h3>
                <br>
                <p>{joke}</p>
                """,
            }
        ]
    }
    result = mailjet.send.create(data=data)
    print(result.status_code)
    # print(result.json())


if __name__ == "__main__":
    joke=get_joke()
    with open('spam.csv', 'r') as csv_file:
        reader = csv.reader(csv_file)
        next(reader)
        for row in reader:
            send_email(row[0], row[1], joke)
